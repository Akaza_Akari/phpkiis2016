<?php
$choices =
[
  [
    ["Starship Trooper","bad"],
    ["Roundabout","good"],
    ["Yours is no disgrace","bad"],
    ["Owner Of A Lonely Heart","bad"]
  ],
  [
    ["大阪","good"],
    ["東京","bad"],
    ["和歌山","bad"],
    ["愛知","bad"]
  ],
  [
    ["GRAPEVINE","bad"],
    ["BUMP OF CHICKEN","good"],
    ["ELLEGARDEN","bad"],
    ["SunSet Swish","bad"]
  ],
  [
    ["『韓国嫁日記』","bad"],
    ["『香港嫁日記』","bad"],
    ["『台湾嫁日記』","bad"],
    ["『中国嫁日記』","good"]
  ],
  [
    ["必要十分条件","bad"],
    ["必要条件","good"],
    ["十分条件","bad"],
    ["交換条件","bad"]
  ],
  [
    ["I am...","good"],
    ["We are...","bad"],
    ["He is...","bad"],
    ["You are...","bad"]
  ],
  [
    ["絆","good"],
    ["なでしこジャパン","good"],
    ["スマートフォン","bad"],
    ["どじょう内閣","bad"]
  ],
  [
    ["日産","good"],
    ["トヨタ","good"],
    ["ベンツ","bad"],
    ["スバル","bad"]
  ],
  [
    ["沢城みゆき","bad"],
    ["福山潤","good"],
    ["佐倉綾音","bad"],
    ["内田真礼","bad"]
  ],
];
 ?>
