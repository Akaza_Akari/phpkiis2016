<?php
session_start();
include_once "question.php";
include_once "choices.php";
$rand = mt_rand(0,7);
if (!isset($_SESSION["win"])){
    $_SESSION["win"] = 0;
    $win = $_SESSION["win"];
}else{
  $win = $_SESSION["win"];
}
if (!isset($_SESSION["lose"])){
    $_SESSION["lose"] = 0;
    $lose = $_SESSION["lose"];
}else{
  $lose = $_SESSION["lose"];
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Quiz</title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <script src="https://code.jquery.com/jquery-3.1.0.slim.min.js"></script>
  <script src="js/bootstrap.js"></script>
  <script type="text/javascript" src="js/jquery.cookie-1.4.1.min.js"></script>
  <script type="text/javascript" src="js/t.min.js"></script>
  <style type="text/css">
  body { padding-top: 100px; }
  </style>
  <!--[if lt IE 9]>
  <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script>
  $(function(){
    $(".question").t({
      speed : 200,
      caret : false
    });
  });
  </script>
</head>
<body>

  <header>
    <nav class="navbar navbar-light bg-faded navbar-fixed-top">
      <div class="container">
        <a href="/" class="navbar-brand">クイズなんとかアカデミー</a>
        <button class="navbar-toggler hidden-sm-up float-xs-right" type="button" data-toggle="collapse" data-target="#navbar-header" aria-controls="navbar-header" aria-expanded="false" aria-label="Toggle navigation"></button>
        <div class="clearfix hidden-sm-up"></div> <!-- fix navbar responsive bug -->
        <ul class="nav navbar-nav">
          <li><a href="out.php">ドロップアウト(終了)</a></li>
        </ul>
      </div>
    </nav>
  </header>


  <div class="container">

    <h1><?php echo '現在'.$win.'勝'.$lose.'敗です！'; ?></h1>
    <hr>
    <div class="jumbotron">
      <h1 class="question"><?=$question[$rand]?></h1>
    </div>
    <div class="row">
      <div class="col-md-3"><a href="<?=$choices[$rand][0][1]?>.php" class="btn btn-primary btn-lg btn-block"><?=$choices[$rand][0][0]?></a></div>
      <div class="col-md-3"><a href="<?=$choices[$rand][1][1]?>.php" class="btn btn-danger btn-lg btn-block"><?=$choices[$rand][1][0]?></a></div>
      <div class="col-md-3"><a href="<?=$choices[$rand][2][1]?>.php" class="btn btn-success btn-lg btn-block"><?=$choices[$rand][2][0]?></a></div>
      <div class="col-md-3"><a href="<?=$choices[$rand][3][1]?>.php" class="btn btn-warning btn-lg btn-block"><?=$choices[$rand][3][0]?></a></div>
    </div>
  </div>


</body>
</html>
