<?php
session_start();

if (!isset($_SESSION["win"])){
    $_SESSION["win"] = 0;
    $win = $_SESSION["win"];
}else{
  $win = $_SESSION["win"];
}
if (!isset($_SESSION["lose"])){
    $_SESSION["lose"] = 0;
    $lose = $_SESSION["lose"];
}else{
  $lose = $_SESSION["lose"];
}

$_SESSION = array();
if (isset($_COOKIE[session_name()])) {setcookie(session_name(), '', time()-42000, '/');}
session_destroy();
?>

<!DOCTYPE HTML>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title>Game Over...</title>
  </head>
  <body>
    <h1>Game Over...</h1>
    <p>
      お疲れ様でした。<br>
      <?php echo $win.'勝'.$lose.'敗で終わりました。'; ?>
      <a href="<?php echo 'http://twitter.com/?status=クイズに挑戦した結果'.$win.'勝'.$lose.'敗でした！'; ?>">[ツイートする]</a>
    </p>
  </body>
</html>
